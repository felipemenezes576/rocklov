#language: pt

Funcionalidade: Cadastro de Anúncios
    Sendo usuário de cadastrado no Rocklov que possui equipamentos musicais
    Quero cadastrar meus equipamentos
    Para que eu possa disponibilizalos para locação


    Contexto: Login
        * Login com "betao@yahoo.com" e "pwd123"


    Cenário: Novo equipo

        Dado que acesso o formulario de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submeto o cadastro desse intem
        Então devo ver esse item no seu Dashboard

    
    Esquema do Cenário: Tentativa de cadastro de anúncios

        Dado que acesso o formulario de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | <foto>      |
            | nome      | <nome>      |
            | categoria | <categoria> |
            | preco     | <preco>     |
        Quando submeto o cadastro desse intem
        Então deve conter a mensagem de alerta: "<saida>"

        Exemplos:
            | foto          | nome             | categoria | preco | saida                                |
            |               | Violão de Nylon  | Cordas    | 150   | Adicione uma foto no seu anúncio!    |
            | clarinete.jpg |                  | Outros    | 250   | Informe a descrição do anúncio!      |
            | mic.jpg       | Microfone Shure  |           | 100   | Informe a categoria                  |
            | trompete.jpg  | trompete Cassico | Outros    |       | Informe o valor da diária            |
            | conga.jpg     | Gonga            | Outros    | abc   | O valor da diária deve ser numérico! |
            | conga.jpg     | Gonga            | Outros    | 100a  | O valor da diária deve ser numérico! |