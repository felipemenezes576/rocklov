class SignupPage
  include Capybara::DSL

  def open
    visit "/signup"
  end

  def create(user)
    find("#fullName").set user[:nome] #encontra o campo nome
    find("#email").set user[:email] #encontra o campo e-mail Faker::Internet.free_email
    find("#password").set user[:senha] #encontra o campo senha

    sleep 10
    click_button "Cadastrar" #clica no botão cadastro
  end
end
