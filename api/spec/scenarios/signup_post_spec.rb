describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end
    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end
  context "usuario ja existe" do
    #Dado que eu tenho um novo usuario
    before(:all) do
      payload = payload = { name: "João da Silva", email: "joao@ig.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      #O email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      #quando faço uma requisição para rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retorna 409" do
      #então deve retorna 409
      expect(@result.code).to eql 409
    end
    it "deve retorna uma menssagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end
  context "Nome obrigatorio" do
    before(:all) do
      payload = { name: "", email: "Silva@yahoo.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])
      Signup.new.create(payload)
      @result = Signup.new.create(payload)
    end
    it "valida status code" do
      expect(@result.code).to eql 412
    end
    it "valida id do usuario" do
      expect(@result.parsed_response["error"]).to eql "required name"
    end
  end

  context "email é obrigatorio" do
    before(:all) do
      payload = { name: " Feernando Papito", email: "", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])
      Signup.new.create(payload)
      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 412
    end
    it "valida id do usuario" do
      expect(@result.parsed_response["error"]).to eql "required email"
    end
  end

  context "password é obrigatorio"
  before(:all) do
    payload = { name: " Feernando Papito", email: "", password: "pwd123" }
    MongoDB.new.remove_user(payload[:email])
    Signup.new.create(payload)
    @result = Signup.new.create(payload)
  end

  it "valida status code" do
    expect(@result.code).to eql 412
  end
  it "valida id do usuario" do
    expect(@result.parsed_response["error"]).to eql "required email"
  end
end
