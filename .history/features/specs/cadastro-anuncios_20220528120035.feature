#language: pt

Funcionalidade: Cadastro de Anúncios
    Sendo usuário de cadastrado no Rocklov que possui equipamentos musicais
    Quero cadastrar meus equipamentos
    Para que eu possa disponibilizalos para locação

    Cenário: Novo equipo

        Dado que eu tenho o seguinte equipamento:
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submeto o cadastro desse intem    
        Então devo ver esse intem no seu Dashboard